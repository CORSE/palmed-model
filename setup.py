#!/usr/bin/env python3

from setuptools import setup, find_packages
import sys


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-") or line.startswith("git+"):
                continue
            reqs.append(line)
    return reqs


setup(
    name="palmed-model",
    version="0.2.2",
    description="Read models from Palmed without installing 1000 dependencies",
    author="CORSE",
    license="LICENSE",
    url="https://gitlab.inria.fr/CORSE/palmed-model",
    packages=find_packages(),
    include_package_data=True,
    package_data={"palmed_model": ["py.typed"]},
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={"console_scripts": []},
)
